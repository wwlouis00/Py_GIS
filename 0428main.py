import os, gzip
from datetime import datetime
from glob import glob
import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt
from typing import List, Tuple, Dict, Union
global chunksize_int, chunksize_cha, chunksize_real 
chunksize_int, chunksize_cha, chunksize_real = 4, 1, 4

left_bottom_point: Tuple[float] = (118.0, 20.0)
distince_per_grid: float = 0.0125
lon_size: int = 441
lat_size: int = 561
shape_of_grid: Tuple[int] = (lat_size, lon_size)

# global chunksize_int, chunksize_cha, chunksize_real 
# chunksize_int, chunksize_cha, chunksize_real = 4, 1, 4

# def read_ck(chunk):
#     for b in chunk:
#         yield b
        
# def byte2int(tmp_list):
#     x = tmp_list
#     return x[0] + 256*x[1]+ (256**2)*x[2]+ (256**3)*x[3]

# def int_var(f):
#     return byte2int(list(read_ck(f.read(chunksize_int))))

# def two_bytes_int_var(f):
#     def _byte2int(x):return x[0] + 256*x[1]
#     return _byte2int(list(read_ck(f.read(2))))

# def chr_var(f):
#     return list(read_ck(f.read(chunksize_cha)))

# def cwb_binary_qpe2arr(binary_file):
#     f = binary_file 
#     # with open(filename, "rb") as f:
#     yyyy,mm,dd,hh,mn,ss,nx,ny,nz = [int_var(f) for i in range(9)]
#     proj = "".join(map(chr, [chr_var(f)[0] for i in range(4)]))
#     map_scale, projlat1, projlat2, projlon, alon, alat = [int_var(f) for i in range(6)]
#     xy_scale,dx,dy,dxy_scale = [int_var(f) for i in range(4)]
#     zht = [int_var(f) for i in range(nz)]
#     z_scale,i_bb_mode = [int_var(f) for i in range(2)]
#     unkn01 = [int_var(f) for i in range(9)]
#     varname =  "".join(map(chr, [chr_var(f)[0] for i in range(20)]))
#     varunit = "".join(map(chr, [chr_var(f)[0] for i in range(6)]))
#     var_scale,missing,nradar = [int_var(f) for i in range(3)]
#     mosradar = "".join(map(chr, [chr_var(f)[0] for i in range(4)]))
#     var = np.array([two_bytes_int_var(f) for i in range(nx*ny)])
#     var = np.flip(var.reshape((ny, nx)), axis=0).astype(np.int16) / var_scale
#     return var


qpe_dir_path = str(os.path.join(os.getcwd(), '2022'))
# print(qpe_dir_path)
qpe_files: List = [ os.path.join(root, f) for root, _, files in os.walk(qpe_dir_path) for f in files]
for root, _, files in os.walk(qpe_dir_path):
    for f in files:
        None
print(qpe_files[1])
qpes = [
    (
        datetime.strptime(str(f.split(root)[1]),"\\ensemble_%Y%m%d%H_G01_0000_qpf.csv"), 
        f
    )
    for f in qpe_files
]
print(qpes)

def get_qpe_file_by_trange(start: datetime, end: datetime, qpefiles: List[Tuple[datetime, str]]) -> List[Tuple[datetime, str]]:
    return sorted(list(filter(lambda t: t[0]>start and t[0]<end, qpefiles)))

def get_target_edge(
    lon_edge: Tuple[float],
    lat_edge: Tuple[float], 
    target_left: float,
    target_bottom: float,
    target_distince: float,
    target_shape: Tuple[int],
)->  Dict[str, Dict[str, Dict[str, Union[int, float]]]]:
    lon_min, lon_max = min(lon_edge), max(lon_edge)
    lat_min, lat_max = min(lat_edge), max(lat_edge)
    print(lon_min,lon_max,lat_min,lat_max)
    target_lon_min, target_lat_min = target_left, target_bottom
    target_lon_max = target_lon_min+target_distince*(target_shape[1]-1)
    target_lat_max = target_lat_min+target_distince*(target_shape[0]-1)
    if ((lon_min < target_lon_min) or (lon_max > target_lon_max)) or ((lat_min < target_lat_min) or (lat_max > target_lat_max)): 
        print(lon_min)
        print(lon_max)
        print(lat_min)
        print(lat_max)
        print(target_lon_min)
        print(target_lon_max)
        print(target_lat_min)
        print(target_lat_max)
        raise Exception(f"{lon_edge, lat_edge} are out of range")

    result = {
        'lon': {
            'min': {
                'idx': 0,
                'value': -1
            },
            'max': {
                'idx': 0,
                'value': -1
            },
        },
        'lat': {
            'min': {
                'idx': 0,
                'value': -1
            },
            'max': {
                'idx': 0,
                'value': -1
            },
        },
    }
    # 從左右兩側查找邊界範圍的index
    lon_min_idx, lon_max_idx = 0, 0
    for i in np.arange(0, target_shape[1], 1):
        current_lon = target_lon_min + target_distince*i
        if current_lon >= lon_min:
            result['lon']['min']['idx'] = i
            result['lon']['min']['value'] = current_lon
            print(current_lon)
            break
    for i in np.arange(target_shape[1], -1, -1):
        current_lon = target_lon_max - target_distince*(target_shape[1] - i)
        if  current_lon <= lon_max:
            result['lon']['max']['idx'] = i
            result['lon']['max']['value'] = current_lon
            print(current_lon)
            break
    
    # 從上下兩側查找邊界範圍的index
    lat_min_idx, lat_max_idx = 0, 0
    for i in np.arange(0, target_shape[0], 1):
        current_lat = target_lat_min + target_distince*i
        if current_lat >= lat_min:
            result['lat']['min']['idx'] = i
            result['lat']['min']['value'] = current_lat
            print(current_lat)
            break
    for i in np.arange(target_shape[0], -1, -1):
        current_lat = target_lat_max -  target_distince*(target_shape[0] - i)
        if current_lat <= lat_max:
            result['lat']['max']['idx'] = i
            result['lat']['max']['value'] = current_lat
            print(current_lat)
            break

    return result

# 透過index選取array範圍
def get_qpe_by_idxs(target: np.ndarray, x_min: int, x_max: int, y_min: int, y_max: int) -> np.ndarray:
    return target[x_min:x_max, y_min:y_max]

# 透過時間區間選取對應qpe檔案
def get_qpe_file_by_trange(start: datetime, end: datetime, qpefiles: List[Tuple[datetime, str]]) -> List[Tuple[datetime, str]]:
    return sorted(list(filter(lambda t: t[0]>start and t[0]<end, qpefiles)))

def load_grid(extent: Tuple[int], image_size: Tuple[int]):
    lat = np.linspace(extent[2], extent[3],
                        num=image_size[0], endpoint=True)
    lon = np.linspace(extent[0], extent[1],
                        num=image_size[1], endpoint=True)
    lons, lats = np.meshgrid(lon, lat)
    grid: Tuple[np.ndarray] = (lats, lons)
    print(grid)
    return grid


def get_qpes(start: datetime, end: datetime, lon_edge: Tuple[int], lat_edge: Tuple[int]) -> np.ndarray:
    targets = get_qpe_file_by_trange(
        start=start,
        end=end,
        qpefiles=qpes,
    )
    edge_result = get_target_edge(
        lon_edge=lon_edge, 
        lat_edge=lat_edge, 
        target_left=left_bottom_point[0], 
        target_bottom=left_bottom_point[1], 
        target_distince=distince_per_grid, 
        target_shape=shape_of_grid,
    )
    grid = load_grid(
        extent=(
            edge_result['lon']['min']['value'], 
            edge_result['lon']['max']['value'], 
            edge_result['lat']['min']['value'], 
            edge_result['lat']['max']['value']
        ),
        image_size=(
            edge_result['lat']['max']['idx']-edge_result['lat']['min']['idx'],
            edge_result['lon']['max']['idx']-edge_result['lon']['min']['idx'],
        ),
    )
    


    # result = []
    # for f in tqdm(targets):
    #     with gzip.open(f[1], 'rb') as fp:
    #         qpe_array = cwb_binary_qpe2arr(fp)
    #         qpe_array = get_qpe_by_idxs(
    #             qpe_array, 
    #             edge_result['lat']['min']['idx'], 
    #             edge_result['lat']['max']['idx'], 
    #             edge_result['lon']['min']['idx'], 
    #             edge_result['lon']['max']['idx'],
    #         )
    #         result += [(f[0], qpe_array)]
    # return {'grid': grid, 'rain': result}



    
example_qpes = get_qpes(start=datetime.strptime('2022083112', '%Y%m%d%H'),end=datetime.strptime('2022083116', '%Y%m%d%H'),
    lon_edge=(120.0, 120.2875), lat_edge=(21.8875, 22.55))

# print(example_qpes['rain'])

# for qpe in example_qpes['rain']:
#     plt.cla()
#     plt.clf()

#     figure, ax = plt.subplots(nrows=1, ncols=1)
#     figure.set_figwidth(10)

#     ax.set_title(qpe[0].strftime("%Y-%m-%dT%H"))
#     img = ax.imshow(qpe[1], vmin=0.0, vmax=20)
#     plt.colorbar(img, ax=ax)

#     plt.show()